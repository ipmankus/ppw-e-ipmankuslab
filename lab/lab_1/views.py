from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Steven Kusuman' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 12, 14) #TODO Implement this, format (Year, Month, Date)
npm = 1706028676 # TODO Implement this
img_src = 'https://avatars3.githubusercontent.com/u/1707491?s=280&v=4'
hobby = 'sleep eat pwn repeat'

mhs_name2 = 'Ikhsanul Akbar Rasyid' # TODO Implement this
curr_year2 = int(datetime.now().strftime("%Y"))
birth_date2 = date(1999, 5, 5) #TODO Implement this, format (Year, Month, Date)
npm2 = 1706979285 # TODO Implement this
img_src2 = 'https://avatars1.githubusercontent.com/u/29397847?s=460&v=4'
hobby2 = 'Main Badminton'

# Create your views here.
def index(request):
    response = {'persons' :[
        {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'img_src': img_src, 'hobby': hobby},
        {'name': mhs_name2, 'age': calculate_age(birth_date.year), 'npm': npm2, 'img_src': img_src2, 'hobby': hobby2}
    ]}
    return render(request, 'index.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
